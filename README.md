# CESSDA Café: Carsten's Coffee Machine

Golang implementation of CESSDA Café Coffee machine using [Gin Gonic](https://github.com/gin-gonic/gin/).

## Installation

Assuming you have go, and the code in you go path,
install the dependencies with [Govendor](https://github.com/kardianos/govendor).

```bash
govendor sync
```

## Execution

Simply run

```bash
go run .
```

and access <http://localhost:1337.>
